# WAVE Documentation (website)

This repo contains [WAVE website](https://rsa-manipulation-testbed.gitlab.io/wave-documentation/).  The website is designed for the static site generator [Hugo](https://gohugo.io/) using the [Docsy theme](https://github.com/google/docsy).

## Branches

`published` is the latest **public-facing** version of the website.  It is automatically published by Gitlab pages.  It is protected and you **cannot** push to it directly.

To update the documentation, create a fork/branch and submit an pull request

# Adding Content

Content is written in [Markdown](https://www.markdownguide.org) and can be edited with your [text editor](https://code.visualstudio.com) of choice.

To check the resulting website, you need to [install Hugo.](https://gohugo.io/getting-started/installing/)  At the top directory, run:

```console
hugo serve
```

This will launch a local web server, which you can view in the browser.  As files are edited, this local copy will automatically update.

Large files (images) are handled by `git-lfs`.

## Documentation

Documentation is in the [content/docs/](content/docs/) directory.

The website organization mirrors the (sub-)folders in the docs directory.

Each doc page should be a **separate directory**.   Within that folder, create a file called `_index.md` and place any extra content (images) in the folder.

The `_index.md` should have the format:

```
---
title: "Long Title!!"
linkTitle: "Short Title"
featured_image: "this_image_goes_at_the_top.jpg"
resource:
 - src: "*.jpg"
---

Page content goes here

## It can use markdown

Including an image uses this magic code

{{< imgproc src="an_image_file.jpg" cmd="Fit" opt="1024x768" >}}
Here's a caption!
{{< /imgproc >}}

```


## Blog posts

News/blog are in the [content/post/](content/post/) directory.

The preferred format is a new directory called `YYYY-MM-DD-some-descriptive-title/`.   Within that directory create a file `index.md`.  It should start with standard "front matter":

```
---
categories: ["navfac", "raven","osb","rov"]
date: 2021-12-08T00:00:00Z
title: "First Tank Test with Raven ROV!"
author: "Aaron Marburg"
featured_image: "post/2021-12-08-raven-rov-first-dunk/DSC_0847.jpg"
summary: "We've successfully completed our first test of the Raven ROV in the UW test tank."
resource:
 - src: "*.jpg"
---

Actual content of the blog post goes here.
```

An entry also be either a single Markdown file named `post/YYYY-MM-DD-some-descriptive-title.md`.

# Roles

## I want to ... contribute content

To do this you need:

* A local installation of Hugo
* `git lfs`

To contribute content:

* clone this repo
* add your content in a freature branch
* check that the resulting content looks good using `hugo serve`
* Check files in with `git` / `git lfs`
* submit a pull request

# License

The site, and underlying media are released under the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.


----
----







[Docsy][] is a [Hugo theme module][] for technical documentation sites, providing easy
site navigation, structure, and more. This **Docsy Example Project** uses the Docsy
theme component as a hugo module and provides a skeleton documentation structure for you to use.
You can clone/copy this project and edit it with your own content, or use it as an example.

In this project, the Docsy theme component is pulled in as a Hugo module, together with other module dependencies:

```console
$ hugo mod graph
hugo: collected modules in 566 ms
hugo: collected modules in 578 ms
github.com/google/docsy-example github.com/google/docsy@v0.7.1
github.com/google/docsy-example github.com/google/docsy/dependencies@v0.7.1
github.com/google/docsy/dependencies@v0.7.1 github.com/twbs/bootstrap@v5.2.3+incompatible
github.com/google/docsy/dependencies@v0.7.1 github.com/FortAwesome/Font-Awesome@v0.0.0-20230327165841-0698449d50f2
```

You can find detailed theme instructions in the [Docsy user guide][].

This Docsy Example Project is hosted on [Netlify][] at [example.docsy.dev][].
You can view deploy logs from the [deploy section of the project's Netlify
dashboard][deploys], or this [alternate dashboard][].

This is not an officially supported Google product. This project is currently maintained.

## Using the Docsy Example Project as a template

A simple way to get started is to use this project as a template, which gives you a site project that is set up and ready to use. To do this:

1. Use the dropdown for switching branches/tags to change to the latest released tag `v0.7.1`

2. Click **Use this template**.

3. Select a name for your new project and click **Create repository from template**.

4. Make your own local working copy of your new repo using git clone, replacing https://github.com/me/example.git with your repo’s web URL:

```bash
git clone --depth 1 https://github.com/me/example.git
```

You can now edit your own versions of the site’s source files.

If you want to do SCSS edits and want to publish these, you need to install `PostCSS`

```bash
npm install
```

## Running the website locally

Building and running the site locally requires a recent `extended` version of [Hugo](https://gohugo.io).
You can find out more about how to install Hugo for your environment in our
[Getting started](https://www.docsy.dev/docs/getting-started/#prerequisites-and-installation) guide.

Once you've made your working copy of the site repo, from the repo root folder, run:

```bash
hugo server
```

## Running a container locally

You can run docsy-example inside a [Docker](https://docs.docker.com/)
container, the container runs with a volume bound to the `docsy-example`
folder. This approach doesn't require you to install any dependencies other
than [Docker Desktop](https://www.docker.com/products/docker-desktop) on
Windows and Mac, and [Docker Compose](https://docs.docker.com/compose/install/)
on Linux.

1. Build the docker image

   ```bash
   docker-compose build
   ```

1. Run the built image

   ```bash
   docker-compose up
   ```

   > NOTE: You can run both commands at once with `docker-compose up --build`.

1. Verify that the service is working.

   Open your web browser and type `http://localhost:1313` in your navigation bar,
   This opens a local instance of the docsy-example homepage. You can now make
   changes to the docsy example and those changes will immediately show up in your
   browser after you save.

### Cleanup

To stop Docker Compose, on your terminal window, press **Ctrl + C**.

To remove the produced images run:

```bash
docker-compose rm
```
For more information see the [Docker Compose documentation][].

## Troubleshooting

As you run the website locally, you may run into the following error:

```console
$ hugo server
WARN 2023/06/27 16:59:06 Module "project" is not compatible with this Hugo version; run "hugo mod graph" for more information.
Start building sites …
hugo v0.101.0-466fa43c16709b4483689930a4f9ac8add5c9f66+extended windows/amd64 BuildDate=2022-06-16T07:09:16Z VendorInfo=gohugoio
Error: Error building site: "C:\Users\foo\path\to\docsy-example\content\en\_index.md:5:1": failed to extract shortcode: template for shortcode "blocks/cover" not found
Built in 27 ms
```

This error occurs if you are running an outdated version of Hugo. As of docsy theme version `v0.7.0`, hugo version `0.110.0` or higher is required.
See this [section](https://www.docsy.dev/docs/get-started/docsy-as-module/installation-prerequisites/#install-hugo) of the user guide for instructions on how to install Hugo.

Or you may be confronted with the following error:

```console
$ hugo server

INFO 2021/01/21 21:07:55 Using config file:
Building sites … INFO 2021/01/21 21:07:55 syncing static files to /
Built in 288 ms
Error: Error building site: TOCSS: failed to transform "scss/main.scss" (text/x-scss): resource "scss/scss/main.scss_9fadf33d895a46083cdd64396b57ef68" not found in file cache
```

This error occurs if you have not installed the extended version of Hugo.
See this [section](https://www.docsy.dev/docs/get-started/docsy-as-module/installation-prerequisites/#install-hugo) of the user guide for instructions on how to install Hugo.

Or you may encounter the following error:

```console
$ hugo server

Error: failed to download modules: binary with name "go" not found
```

This error occurs if you have not installed the `go` programming language on your system.
See this [section](https://www.docsy.dev/docs/get-started/docsy-as-module/installation-prerequisites/#install-go-language) of the user guide for instructions on how to install `go`.


[alternate dashboard]: https://app.netlify.com/sites/goldydocs/deploys
[deploys]: https://app.netlify.com/sites/docsy-example/deploys
[Docsy user guide]: https://docsy.dev/docs
[Docsy]: https://github.com/google/docsy
[example.docsy.dev]: https://example.docsy.dev
[Hugo theme module]: https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme
[Netlify]: https://netlify.com
[Docker Compose documentation]: https://docs.docker.com/compose/gettingstarted/
