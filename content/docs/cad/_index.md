---
title: "CAD Models & Drawings"
linkTitle: "CAD Models & Drawings"
weight: 4
description: >
  All 3D modeling was completed in Solidworks 2022
---

![CAD rendering of full WAVE assmebly](cad_render.jpg)
