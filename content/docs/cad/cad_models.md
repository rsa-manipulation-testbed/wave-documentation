---
title: "CAD Models"
linkTitle: "CAD Models"
weight: 3
description: >
  Access the CAD models here
---

To access WAVE CAD follow these steps:
1. Download *Testbed2pt0.zip* from [here](https://gitlab.com/rsa-manipulation-testbed/testbed-cad/-/blob/main/Testbed2pt0.zip?ref_type=heads)
2. Extract its contents to a desired directory
3. Access the full assembly through *Testbed2pt0.SLDASM*

Some machinist drawings are saved as *.pdf*s while others still remain as *.SLDDRW*s.
