---
title: "Bill of Materials"
linkTitle: "Bill of Materials"
weight: 5
description: >
  This is where I will format a BOM table
---


<!-- <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Description</th>
      <th scope="col">QTY</th>
      <th scope="col">Vendor and P/N</th>
      <th scope="col">Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
      <td>@marcus</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      <td>@bigboi</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2">Larry the Bird</td>
      <td>@twitter</td>
      <td>@mdo</td>
    </tr>
  </tbody>
</table> -->


<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><link type="text/css" rel="stylesheet" href="resources/sheet.css" >
<style type="text/css">.ritz .waffle a { color: inherit; }.ritz .waffle .s1{background-color:#ffffff;text-align:left;color:#000000;font-family:'docs-Open Sans',Arial;font-size:12pt;vertical-align:middle;white-space:nowrap;direction:ltr;padding:2px 3px 2px 3px;}.ritz .waffle .s0{background-color:#f3f3f3;text-align:left;font-weight:bold;color:#000000;font-family:'docs-Open Sans',Arial;font-size:12pt;vertical-align:middle;white-space:nowrap;direction:ltr;padding:2px 3px 2px 3px;}.ritz .waffle .s2{background-color:#ffffff;text-align:left;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;color:#1155cc;font-family:'docs-Open Sans',Arial;font-size:12pt;vertical-align:middle;white-space:nowrap;direction:ltr;padding:2px 3px 2px 3px;}</style><div class="ritz grid-container" dir="ltr"><table class="waffle" cellspacing="0" cellpadding="0"><thead><tr><th class="row-header freezebar-origin-ltr"></th><th id="0C0" style="width:425px;" class="column-headers-background">A</th><th id="0C1" style="width:58px;" class="column-headers-background">B</th><th id="0C2" style="width:217px;" class="column-headers-background">C</th><th id="0C3" style="width:596px;" class="column-headers-background">D</th></tr></thead><tbody><tr style="height: 20px"><th id="0R0" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">1</div></th><td class="s0">Name</td><td class="s0">Qty</td><td class="s0">Part No.</td><td class="s0">Notes</td></tr><tr style="height: 20px"><th id="0R1" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">2</div></th><td class="s1" dir="ltr">ClearPath - Integrated Servo System (For CoreXY)</td><td class="s1">2</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/model-info/CPM-SCSK-3432S-RLSB/?model_voltage=75">CPM-SCSK-3432S-RLSB</a></td><td class="s1" dir="ltr">Shaft Seal = Yes, Enhanced Option = Regular, Firmaware Option = Basic</td></tr><tr style="height: 20px"><th id="0R2" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">3</div></th><td class="s1" dir="ltr">ClearPath - Integrated Servo System (For Yaw)</td><td class="s1">1</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/model-info/CPM-SCSK-2311S-RLSB/">CPM-SCSK-2311S-RLSB</a></td><td class="s1" dir="ltr">Shaft Seal = Yes, Enhanced Option = Regular, Firmaware Option = Basic</td></tr><tr style="height: 20px"><th id="0R3" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">4</div></th><td class="s1" dir="ltr">ClearPath - Integrated Servo System (For Z-axis)</td><td class="s1">1</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/model-info/CPM-SCHP-2315P-ELSB/">CPM-SCHP-2315P-ELSB</a></td><td class="s1" dir="ltr">Shaft Seal = Yes, Enhanced Option = Regular, Firmaware Option = Basic</td></tr><tr style="height: 20px"><th id="0R4" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">5</div></th><td class="s1" dir="ltr">DC Power Distribution Hub</td><td class="s1">1</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/POWER4-HUB/">POWER4-HUB</a></td><td class="s1"></td></tr><tr style="height: 20px"><th id="0R5" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">6</div></th><td class="s1" dir="ltr">Communication Hub for ClearPath-SC Series</td><td class="s1">1</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/SC4-HUB/">SC4-HUB</a></td><td class="s1"></td></tr><tr style="height: 20px"><th id="0R6" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">7</div></th><td class="s1" dir="ltr">Controller Cable, 10 ft</td><td class="s1" dir="ltr">4</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/CPM-CABLE-CTRL-MU120/">CPM-CABLE-CTRL-MU120</a></td><td class="s1">10ft w/ molded boot</td></tr><tr style="height: 20px"><th id="0R7" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">8</div></th><td class="s1" dir="ltr">DC Power Cable, IPC to ClearPath, 10 ft</td><td class="s1" dir="ltr">5</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/CPM-CABLE-PWR-MS120/">CPM-CABLE-PWR-MS120</a></td><td class="s1">10ft</td></tr><tr style="height: 20px"><th id="0R8" style="height: 20px;" class="row-headers-background"><div class="row-header-wrapper" style="line-height: 20px">9</div></th><td class="s1" dir="ltr">24 Volt Power, 2-Pin Molex Cable</td><td class="s1">1</td><td class="s2" dir="ltr"><a target="_blank" href="https://teknic.com/CPM-CABLE-M2P2P-120/">CPM-CABLE-M2P2P-120</a></td><td class="s1">10ft</td></tr></tbody></table></div>
