---
title: "Bravo Manipulator Driver"
linkTitle: "Bravo Manipulator Driver"
weight: 2
description: >
  Software installation for the Bravo Manipulator
---

Follow the steps listed in the [README.md](https://gitlab.com/rsa-manipulation-testbed/motion_primitives) for intial usage of the Bravo manipulator.
