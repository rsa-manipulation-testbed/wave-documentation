---
title: "Software"
linkTitle: "Software"
weight: 3
description: >
  The testbed software communicates with the testbed hardware.
---

The majority of the testbed control software is ROS-based, although there are some peripheral utilities which run outside of ROS for simplicity. The ROS-based software driver speaks to the motors and interfaces with the [ROS Control](http://wiki.ros.org/ros_control) stack to allow the testbed to be driven by tools like [MoveIt!](https://moveit.ros.org/).

The file software archiecture is described in the detailed software description

The core Testbed software is stored in two Gitlab repos:
* [testbed_hw](https://gitlab.com/rsa-manipulation-testbed/testbed_hw) is concerned with driving the actual Testbed, including communicating with the Clearpath motors.
* [testbed_sw](https://gitlab.com/rsa-manipulation-testbed/testbed_sw) contains configuration files which describe and configure the testbed. Some configuration is specific to the hardware testbed (for example, which Clearpath motors drive which axes). Other configuration files are used for Gazebo simulation, and some are used for for hardware and sim operation.

![Testbed Software](img/TestbedSoftware.png)
