---
title: "Testbed Software Installation"
linkTitle: "Testbed Software Installation"
weight: 1
description: >
  Software installation for the testbed
---

## Install the ROS software
*These instructions assume you have at least a basic ROS install, C/C++ compilers, etc*

Get prerequisities:
```bash
apt install -y python3-vcstool
```

On a system running ROS Noetic, create a ROS workspace and checkout out ```testbed_hw```:
```bash
mkdir -p testbed_ws/src
cd testbed_ws/src
git clone https://gitlab.com/rsa-manipulation-testbed/testbed_hw
```

Then checkout the other dependencies using the ```.rosinstall``` file found in that repo, then use ```rosdep``` to get any other dependencies
```bash
cd ..
vcs import testbed_hw/testbed_hw.rosinstall
rosdep install --ignore-src --from-paths . -y
```

Then build:
```bash
catkin build
```

## Install the Clearpath Device Driver
The Clearpath communication hub requires a special device driver. This must be built and installed manually any time the system kernel is updated. If the driver is not installed, the system hub will show up as a ```/tty/devACMx``` device. If installed properly, the hub will show up as a ```/dev/ttyXRUSBx``` device.

A short script [```install_kernel_modules.sh```](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/blob/main/clearpath_sc_ros/install_kernel_module.sh) is included in ```testbed_hw/clearpath_sc_ros``` which will automate this building process. It can be run from a checkout of tha repo or:
```bash
wget -O - https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/raw/main/clearpath_sc_ros/install_kernel_module.sh | /bin/bash -
```

## Configuration
Most system configuration is centralized in the [```testbed_sw```](https://gitlab.com/rsa-manipulation-testbed/testbed_sw) repository. This repo contains multiple sub-packages:

* [```testbed_config```](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_config) contains testbed hardware and controller configurations.
* [```testbed_description/```](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_description/) contains the URDF and meshes for the testbed.
* [```testbed_moveit_config/```](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_moveit_config/) contains configuration for MoveIt! and Gazebo for the testbed.

Most of the configuration should be set to appropriate default values, with the exception of the Clearpath motor mapping, which must be set for each Testbed site.

## Motor mapping
The software must be told explicitly which Clearpath motors correspond to which testbed axes. This is done through a config file in [```testbed_config```](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_config/config) called ```{TESTBED_SITE}_config.yaml``` where ```{TESTBED_SITE}``` is a descriptive name for a given testbed installation.

The file contains four config params which give the Clearpath serial number fo the for axis. For example ```uwapl_config.yaml``` is:
```bash
#
# Clearpath motor serial number for APL Testbed
#
yaw_sn: 70403992
zaxis_sn: 73003302
corexy_a_sn: 65800963
corexy_b_sn: 66102023
```

The [```testbed.launch```](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/blob/main/testbed_driver/launch/testbed.launch) assumes that the argument ```testbed_site``` is set, or as a default will look for an environment variable ```TESTBED_SITE```

*!!! We strongly suggest that the environment variable* ```TESTBED_SITE``` *be set automatically on the user account/computer which drives the testbed.*

## Clearpath motor configuration
The [```clearpath_config.yaml```](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/blob/main/testbed_config/config/clearpath_config.yaml) file sets motor paramters **within the Clearpath motor itself.** This includes velocity, acceleration, current and jerk limits. As these limits are set in the motors they are **absolute** limits on motor performance.

## ROS Controller configuration and tuning
The [```testbed_controllers.yaml```](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/blob/main/testbed_config/config/testbed_controllers.yaml) file describes the motor control interfaces exposed by the Testbed to the ROS Control system. Since the testbed uses position control loops which run **within ROS** this includes PID parameters for those control loops, as well as joint limits to be enforced by the software controllers.
