---
title: "Tuning Testbed Motors"
linkTitle: "Tuning Testbed Motors"
weight: 3
description: >
  Download Clearpath software and run the auto-tune software
---

Teknic offers a software API to interface directly with their integrated servo motors. Amongst many actions within the software, there is an *Auto-tune* command sequence that enables tuning of individual motors based on their mechanical setup.

Download the ClearView software by:
1. Registering an email with Teknic to access the software and documentation.
2. This software can be found on the [Teknic Downloads page](https://teknic.com/downloads/).
3. Clicking the dropdown menus labeled *ClearPath/Software/ClearPath-SC Series Motor Setup...*
4. Download and install the ClearView software for your respective computer operating system.
5. There is also the option to download and install the stand-alone USB port for communicating with the ClearPath-SC motors if not using a computer that does not have ClearView installed. An instance of this installation is performed in the *Software/Testbed Software Installation* instructions.

Run the Auto-tune command:
1. Plug the Comms board into your computer.
2. Power on the servo motors and Comms board.
3. Open ClearView.
4. Select the motor you want to tune.
5. In the upper-menu boarder, select the dropdown menu *Setup*.
6. Select *Auto-tune...*.
7. Follow the API instructions to complete this function.

*Be aware that the auto-tune function will toggle motion on the selected servo. Be sure to stay away from any moving parts and that the servo axis is clear of any obstructions to avoid collisions.*
