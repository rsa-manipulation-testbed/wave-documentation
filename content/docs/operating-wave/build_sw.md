<!-- ---
title: "Building software"
linkTitle: "Building software"
weight: 3
description: >
  Sourcing the software to run estbed
--- -->

The ROS testbed driver runs as a single node. To run the software:

1. Install the software using catkin and add it to your workspace:
```bash
cd testbed_ws
catkin build
source devel/setup.bash
```

2. Ensure the ```TESTBED_SITE``` environment variable is set and that there’s a corresponding ```${TESTBED_SITE}_config.yaml``` which gives the Clearpath motor serial numbers.

3. Run the testbed launchfile:
```bash
roslaunch testbed_driver testbed.launch
```

By default the driver runs all four axes but individual axes can be disable if they are not available:
```bash
roslaunch testbed_driver testbed.launch enable_yaw:=false
```
