<!-- ---
title: "Testbed Gamepad Control"
linkTitle: "Testbed Gamepad Control"
weight: 3
description: >
  Run teleoperation mode of the testbed
--- -->

### Launching the joystick node
A simple [joystick-based remote control node](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/tree/main/testbed_teleop) is provided. Right now we are using Logitech joysticks, the axis mapping may be different on XBox controllers.

1. In a separate window, source the Testbed workspace:
```bash
source devel/setup.bash
```

2. Run the teleop node:
```bash
roslaunch testbed_teleop teleop.launch
```

*When done, **ctrl-C** to quit.*

### Operating the game controller
In the current version, all four axes start in a velocity control mode, with all axes disabled.
1. The left shoulder button enables all axes.
2. The right shoulder button disables all axes.
3. The left joystick controls the CoreXY axes.
4. The right joystick (left-right) controls the Yaw axis.
5. The right joystick (up-down) controls the Z-axis.
6. The start button switches the system between velocity and position mode. The initial press will switch the system to position mode. This mode is highly untested and will cause the testbed to immediately try to return to zero position … **!!do not use!!**. Pressing start again will put the unit in velocity mode.

![Game control image](/static/img/game_control.png)
