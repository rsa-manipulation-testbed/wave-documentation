---
title: "Using WAVE"
linkTitle: "Using WAVE"
weight: 1
description: >
  Instructions on how to operated the WAVE
---
The current version of the testbed does not have software/hardware limits on axes travel. When driving the physical testbed, be away of the travel of each axis to ensure no crashing at the end of their travel.

## Build the testbed software
The ROS testbed driver runs as a single node. To run the software:

1. Install the software using catkin and add it to your workspace:
    ```bash
    cd testbed_ws
    catkin build
    source devel/setup.bash
    ```

2. Ensure the ```TESTBED_SITE``` environment variable is set and that there’s a corresponding ```${TESTBED_SITE}_config.yaml``` which gives the Clearpath motor serial numbers.

3. Run the testbed launchfile:
    ```bash
    roslaunch testbed_driver testbed.launch
    ```

By default the driver runs all four axes but individual axes can be disable if they are not available:
    ```bash
    roslaunch testbed_driver testbed.launch enable_yaw:=false
    ```

## Teleoperating the testbed
### Launching the joystick node
A simple [joystick-based remote control node](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/tree/main/testbed_teleop) is provided. Right now we are using Logitech joysticks, the axis mapping may be different on XBox controllers.

1. In a separate window, source the Testbed workspace:
    ```bash
    source devel/setup.bash
    ```

2. Run the teleop node:
    ```bash
    roslaunch testbed_teleop teleop.launch
    ```

*When done, **ctrl-C** to quit.*

### Operating the game controller
In the current version, all four axes start in a velocity control mode, with all axes disabled.
1. The left shoulder button enables all axes.
2. The right shoulder button disables all axes.
3. The left joystick controls the CoreXY axes.
4. The right joystick (left-right) controls the Yaw axis.
5. The right joystick (up-down) controls the Z-axis.
6. The start button switches the system between velocity and position mode. The initial press will switch the system to position mode. This mode is highly untested and will cause the testbed to immediately try to return to zero position … **!!do not use!!**. Pressing start again will put the unit in velocity mode.

![Game control image](img/game_control.png)

## Zeroing Testbed Motors
The current method for zeroing the testbed is in its beta phase and is as follows:
1. Identify a location within the physical testbed workspace that you would like to set as your "zero" or "home" position (i.e. bottom left corner, bottom right corner, etc).
2. Identify a z-height that you would like your "zero" to be.
3. Identify a yaw "zero" orientation.
4. Enable the testbed.
5. Use the controller to manually drive the testbed to the identified "zero" location (X, Y, Z, Yaw).
6. In another terminal enter the following command for the desired ```axis``` value: ```yaw```, ```zmotor```, or ```corexy``` (which does both motors).
      ```bash
      rosrun testbed_driver tbc zero {axis}
      ```
