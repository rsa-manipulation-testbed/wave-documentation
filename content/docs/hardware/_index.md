---
title: "Hardware"
linkTitle: "Hardware"
weight: 2
description: >
  See the below sections for further detail about each subcomponent of the WAVE testbed
---
![System Architecture](img/testbed_system_architecture.png)

## Core-XY
To generate motion in the XY-plane, the testbed uses a Core-XY belt/pulley transmission system to drive a central carriage along linear rails.

With the Core-XY system, individual actuator motion (i.e. one servo rotating and one servo fixed) generates diagonal carriage movement and paired actuation motion generates coordinated “left”, “right”, “forward”, or “backward” movements. Compared to a traditional gantry design, the Core-XY system allows the larger X-Y actuators to remain fixed to the frame, reducing the mass of the moving portion of the system.

The XY frame moves the mobile trolley carriage that includes an electrical junction box, the Yaw and Z-axes, and a suspended aluminum frame with the underwater manipulator and perception sensor.

The XY frame is also mounted to four welded square aluminum legs with slotted feet for anchoring to the floor which results in an overall testbed dimension of *1.95 m x 1.83 m x 1.55 m*.

![Z & Yaw axes](img/z_and_yaw_less_reflective.JPG)


## Yaw
The yaw axis is housed within the mobile carriage and its servo motor rotates a slewing ring through a 5:1 belt and pulley system, which in turn rotates the vertical column.


## Z-Axis
Within the vertical column is the Z-axis servo motor, which drives the Z motion stage (with the manipulator and perception sensor) using a leadscrewwith a total travel length of *0.75 m*.

The actuator integrates with the leadscrew via a 5:1 planetary gearbox and flexible coupling to ensure smooth motion of heavy payloads attached to this motion stage.


## Compliance
A key feature of WAVE is physical embodiment of realistic dynamic coupling between the manipulator and vehicle body through passive compliance. To design a compliant pitch mechanism for WAVE, we constructed a rigid body model of an idealized ROV with a manipulator in a Julia based hydrodynamic simulation.

Within this simulation, the dynamic coupling between the vehicle and manipulator were studied by evaluating the stiffness interaction on the vehicle imposed by the manipulator. The simulation inputs were the manipulator's collision free planar workspace, and the output was the manipulator's configuration based center of mass (COM) and the vehicle's resultant pitch at equilibrium.

The resultant simulated stiffness parameters drove mechanical design for pitch compliance.

![Compliance Mechanical Design FBD](img/compliance_mechanical_design.png)
