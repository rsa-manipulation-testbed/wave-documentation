<!-- ---
title: "Compliance"
linkTitle: "Compliance"
weight: 4
description: >
  Detailed Compliance Description
---

A key feature of WAVE is physical embodiment of realistic dynamic coupling between the manipulator and vehicle body through passive compliance. To design a compliant pitch mechanism for WAVE, we constructed a rigid body model of an idealized ROV with a manipulator in a Julia based hydrodynamic simulation.

Within this simulation, the dynamic coupling between the vehicle and manipulator were studied by evaluating the stiffness interaction on the vehicle imposed by the manipulator. The simulation inputs were the manipulator's collision free planar workspace, and the output was the manipulator's configuration based center of mass (COM) and the vehicle's resultant pitch at equilibrium.

The resultant simulated stiffness parameters drove mechanical design for pitch compliance. -->

<!-- ![Compliance Mechanical Design FBD](compliance_mechanical_design.png) -->

<!-- ```math
\tag*{(1)} M \mathcal{\dot V} + C(\mathcal{V})\mathcal{V} + D(\mathcal{V})\mathcal{V} + g(\eta) = \tau
``` -->
