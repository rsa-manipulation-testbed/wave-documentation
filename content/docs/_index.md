
---
title: Documentation
linkTitle: Documentation
menu: {main: {weight: 20}}
---

![Physical Testbed](img/testbed_abstract.png)

WAVE is a 10-degree-of-freedom underwater manipulation platform that uses [Teknic ClearPath Integrated Servos Motors](https://teknic.com/products/clearpath-brushless-dc-servo-motors/), [Reach Robotics Bravo Manipulator](https://reachrobotics.com/products/manipulators/reach-bravo/) and [UW-APLs Trisect](https://trisect-perception-sensor.gitlab.io/trisect-docs/) perception sensor.

## **Design Fundamentals**
WAVE is a kinematically redundant system consisting of a 4-DOF positioning gantry carrying a 6-DOF electric subsea manipulator and steovision sensor. All components beneath the XY-frame are waterproofed such that they can all be submerged and used in underwater manipulation studies.

The positioning gantry is designed such that it replicates motions typically seen on inspection class remotely operated vehicle (ROV). With repsective motions being generated in the surge-sway-heave-yaw directions and each axis being powered by a encoder based servo motor. With the addition of a manipulator and perception sensor, the system as a whole can further replicate a full UVMS.

We also incorporated an additional DOF in the pitch direction by designing a compliant mechanism that closely replicates the dynamic coupling effects seen between a manipulator and vehicle body on a UVMS.

## Advantages of WAVE
* Each servo motor on the testbed frame can be individually tuned based on the respective load on its shaft. See [Tuning Testbed Motors]({{<ref "tuning_motors" >}}) for more information.

* If submersion of the WAVE system is not possible, the passively compliant component characteristics can be modified to compliment a dry lab setup. See [Compliance]({{<ref "pitch_compliance" >}}) for more information.

* WAVE can maintain accurate ground truth localization through encoders at each of its joints.


## Disdvantages of WAVE
* Common UVMS have two passively stable unactuated DOF: pitch and roll. The current version of WAVE only includes passive compliance in the pitch axis.
