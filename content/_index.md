---
title: WAVE - An open-source underWater Arm-Vehicle Emulator
# description: An open-source underWater Arm-Vehicle Emulator
---

{{% blocks/cover image_anchor="top" height="max" color="primary"%}}
![Logo test](osu_apl_wave.svg)
{.otel-logo}

{{% param description %}}
{.display-6}

{{% blocks/link-down color="info" %}}
{{% /blocks/cover %}}

{{% blocks/lead color="primary" %}}
WAVE is a novel physical testbed developed for underwater manipulation studies.

Read about: the <a href="docs/">Testbed Overview</a>, <a href="docs/hardware/">Hardware</a>, and <a href="docs/software/">Software</a>.

See the <a href="docs/bom/">bill of materials</a> and <a href="docs/cad/">3D models</a> to build your own.

{{% /blocks/lead %}}

{{% blocks/section type="section" color="dark" type="row" %}}

### Key capabilities:

* Replicated ROV motion: surge-sway-heave-yaw within an approximately 2m x 2m x 1m square workspace
* Ground truth localization: feedback from joint encoders at each axis can be used to accurately reconstruct
the forward kinematics of the testbed and provide  accurate state estimation
* Two operating modes: rigid and passively compliant. In the passive-mode, the ROV body can pitch similar to
how an underactuated vehicle without pitch control would rotate during manipulatormotion due to dynamic coupling.
* Modularity: the testbed can be disassembled and re-assembled within two hours

{{% /blocks/section %}}
